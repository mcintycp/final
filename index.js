
var zipURL = "https://api.clearllc.com/api/v2/miamidb/_table/zipcode"
var weatherURL = "https://api.openweathermap.org/data/2.5/onecall"
var weather2 = 'https://api.openweathermap.org/data/2.5/weather'
var apiKEY = "bed859b37ac6f1dd59387829a18db84c22ac99c09ee0f5fb99cb708364858818";
var openWeatherKey = "3a3799bbc1479a3f68aed6fd29f75ac4";


$('.collapses').on('show.bs.collapse', '.collapse', function () {
    $('.collapses').find('.collapse.show').each(function () {
        $(this).toggleClass('show');
    });
});

$('#weatherWeekList').css('height', `${~~(screen.availHeight / 2.2)}px`);
$(window).resize(function() {
    $('#weatherWeekList').css('height', `${~~(screen.availHeight / 2.2)}px`);
    // console.log('working');
});


$('#weatherForm').submit(function () {
    submitZip();
    return false;
});



function updateTitle(num) {
    switch (num) {
        case 1:
            $('#title').text('Weather Forecast');
            break;
        default:
            $('#title').text('Student Information');
            break;
    }
}


function submitZip() {
    let date = new Date();
    let weekStart = new Date();
    let weekEnd = new Date();
    weekStart.setDate(date.getDate() + 1);
    weekEnd.setDate(date.getDate() + 8);
    $('#dateToday').text(`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`);
    $('#dateWeek').text(`${weekStart.getFullYear()}-${weekStart.getMonth() + 1}-${weekStart.getDate()} - ${weekEnd.getFullYear()}-${weekEnd.getMonth() + 1}-${weekEnd.getDate()}`)
    $.ajax({
        url: `${zipURL}?api_key=${apiKEY}&ids=${$('#zip').val()}`,
        method: 'GET'
    }).done(function(data) {
        let lat = data.resource[0].latitude
        let lon = data.resource[0].longitude
        $.ajax({
            url: `${weatherURL}?lat=${lat}&lon=${lon}&exclude=hourly&appid=${openWeatherKey}`,
            method: 'GET'
        }).done(function(data) {
            let daily = data.daily
            $('#weatherWeekList').html("");
            daily.forEach(day => {
                let date = new Date(day.dt * 1000);
                console.log(day);
                $('#weatherWeekList').append(`<li class="list-group-item bg-transparent text-body"><div class="row justify-content-around"><div class="col margined">${dayToDay(date.getDay())} (${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()})</div><div class="col text-right margined">${toFahrenheit(day.temp.max)}°-${toFahrenheit(day.temp.min)}°F<img src="https://openweathermap.org/img/wn/${day.weather[0].icon}.png" width="30px">></div</div></li>`);
            });
            $('#weatherResults').show();
        }).fail(function(err) {            
        });
    }).fail(function(err) {
        $('#weatherWeekList').html(`Please make sure that the zip code is correct`);
        $('#weatherResults').show();
    });
}

function dayToDay(num) {
    switch(num) {
        case 0:
            return "Sun"
        case 1:
            return "Mon"
        case 2:
            return "Tue"
        case 3:
            return "Wed"
        case 4:
            return "Thu"
        case 5:
            return "Fri"
        default:
            return "Sat"
    }
}

function toFahrenheit(num) {
    return Math.floor((num - 273.15) * 9 / 5 + 32);
}
